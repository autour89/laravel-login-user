@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Landing</div>

                <div class="panel-body">
                    @foreach ($actions as $action)
                        <a href="">{{ $action->id }}. {{ $action->name }},  {{ $action->email }}</a>
                        <a href="{{ route('edit',['action' => $action->id ]) }}" class = "btn btn-default btn-lg" >Edit</a>
                        <br/><br/>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
