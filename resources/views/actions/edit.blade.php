@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit User</div>
                <div class="panel-body">
                    <form action="{{ route('upd_user') }}" method="post">
                        
                        <label for="Name" class="col-md-4 control-label">Name</label>
                        <input type="text" class="form-control" name="name" id="name" value="{{ $user->name }}"/><br><br>
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                        <input type="text" class="form-control" name="email" value="{{ $user->email }}"/><br/><br>
                        <input type="hidden" name="id" value="{{ $user->id }}">
                        <br>
                        <button type="submit" class="btn btn-primary">Save</button>
                        
                        <input type="hidden" value="{{ Session::token() }}" name="_token">
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


