CREATE DATABASE AuthtnDB;
use AuthtnDB;
drop database AuthtnDB;



CREATE TABLE Users (
  id INT(10) not null auto_increment primary key,
  name VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  remember_token VARCHAR(100) NULL,
  created_at timestamp null,
  updated_at timestamp null
)ENGINE=InnoDB;

     
  