<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::auth();

Route::group(['middleware' => ['web']], function(){
    
    Route::get('/',[
        'uses' => 'HomeController@index',
        'as' => 'home'
    ]);

    Route::get('/home', 'HomeController@index');

    Route::group(['prefix' => 'do'], function(){
        Route::get('/{action}/{id?}', [
            'uses' => 'HomeController@editUser',
            'as' => 'edit'
        ]);

        Route::post('/upd_user', [
            'uses' => 'HomeController@postUpdateUser',
            'as' => 'upd_user'
        ]);
    });  
    
});  