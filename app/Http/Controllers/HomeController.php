<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use DB;
use Auth;
use App\User;
use Crypt;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // if(Auth::check()){
        //     $actions = User::all();
        //     return view('home',['actions' => $actions]);
        // }
        // else
        //     return view('/');

        $actions = User::all();
        return view('home',['actions' => $actions]);
    }

    public function editUser($id=null)
    {
        $user = User::where('id', $id)->first();
        //$user = DB::table('users')->whereId($id)->first();
        // $decrypted = Crypt::decrypt($user->password);
        return view('actions/edit', ['user' => $user]);
    }

    public function postUpdateUser(Request $request)
    {
        $user = User::find($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        // DB::table('users')
        //     ->where('id', $request->id)
        //     ->update(['fname' => $request->fname, 'email' => $request->email]);
        return redirect()->route('home');
    }

}
